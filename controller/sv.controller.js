renderDSSV = function (dssv) {
  let contentHTML = "";

  dssv.forEach((sv) => {
    var contentTr = `


   <tr>
   <td>${sv.id}</td>
   <td>${sv.name}</td>
   <td>${sv.email}</td>
   <td>${Math.floor((sv.math+sv.physics+sv.chemistry)/3)}</td>
   <td>
   <button 
   onclick=xoaSinhVien('${sv.id}')
   class="btn btn-danger">Xoá</button>
   <button onclick=editSV('${sv.id}') class="btn btn-warning">Sửa</button>
   </td>
   </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function getSVInforFromForm() {
  var id = document.getElementById("txtMaSV").value;
  var name = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var password = document.getElementById("txtPass").value;
  var math = document.getElementById("txtDiemToan").value*1;
  var physics = document.getElementById("txtDiemLy").value*1;
  var chemistry = document.getElementById("txtDiemHoa").value*1;

  return new SV(
    id,
    name,
    email,
    password,
    math,
    physics,
    chemistry
  );
}

function showSVInforToForm(sv) {
  document.getElementById("txtMaSV").value = sv.id;
  document.getElementById("txtTenSV").value = sv.name;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.password;
  document.getElementById("txtDiemToan").value = sv.math;
  document.getElementById("txtDiemLy").value = sv.physics;
  document.getElementById("txtDiemHoa").value = sv.chemistry;
}

