const BASE_URL = "https://62e4cfc7c6b56b45118a1b7c.mockapi.io/";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

getDSSV();

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function updateSV() {
  let sv = getSVInforFromForm();

  batLoading();
  axios({
    url: `${BASE_URL}/sv/${sv.id}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      getDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function themSV() {
  // newSv = object lấyb từ form
  let newSV = {
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    password: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value*1,
    physics: document.getElementById("txtDiemLy").value*1,
    chemistry: document.getElementById("txtDiemHoa").value*1,
  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function editSV(id){
  batLoading();
   
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      var sv = res.data;

      showSVInforToForm(sv);

    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
